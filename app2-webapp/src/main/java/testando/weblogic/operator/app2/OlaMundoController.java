package testando.weblogic.operator.app2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OlaMundoController {

    @GetMapping("olamundo")
    @ResponseBody
    public String olaMundo(){
        return "Olá Mundo!";
    }

    @GetMapping("olamundo-shared")
    @ResponseBody
    public String olaMundoShared(){
        return new OlaMundo().olamundo();
    }

}
