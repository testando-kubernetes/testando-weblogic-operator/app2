package testando.weblogic.operator.app2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.WebApplicationInitializer;

@SpringBootApplication
public class App extends SpringBootServletInitializer implements WebApplicationInitializer {
//    @Bean
//    public HandlerAdapter handlerAdapter() {
//        return new MyHandlerAdapter();
//    }
//
//    @Bean
//    @Primary
//    public HandlerMapping handlerMapping() {
//        return new MyHandlerMapping();
//    }

//    @Bean
//    @Primary
//    public RequestMappingHandlerMapping requestMappingHandlerMapping(){
//        return new MyUrlHandlerMapping();
//    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }


    @Override
    protected SpringApplicationBuilder createSpringApplicationBuilder() {
        return super.createSpringApplicationBuilder()
                .sources(this.getClass());
    }
}
